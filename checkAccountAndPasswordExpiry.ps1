﻿Function checkAccountAndPassword {

    $result = @{}
    $lockedOutStatus = (Get-ADUser -Identity  $env:username -Properties lockedout, passwordexpired).lockedout
    $passwordExpiredtStatus = (Get-ADUser -Identity  $env:username -Properties passwordexpired).passwordexpired
    
    # Default result
    $result["Pass"] = $true 
    
    If($lockedOutStatus) {
        $result["Pass"] = $false 
        $result["Result"] = "Your account is locked. Please ring Helpdesk. "
    }

    If($passwordExpiredtStatus) {
        $result["Pass"] = $false 
        $result["Result"] += "Your password has expired. Please change it. Press Ctl+Alt+Del and Click 'Change Password'."
    }

    If($result["Pass"] -eq $true) {
       $result["Result"] = "Your account is not locked and password is not expired."
    }

    Return $result
}
