﻿Function getDHCPServer {
    $result = @{}

    $test = ipconfig /all

    $result["Pass"] = $false
    $result["Result"] = "No DHCP Server found"
    
    ForEach($line In $test) {
        If($line -Like "*DHCP Server*") {
            $result["Pass"] = $true
            $result["Result"] = $line.Replace(":","").Replace(". ","").Replace("DHCP Server ","<font color='black'>Address:</font>").Trim()
        }
    }

    Return $result
}