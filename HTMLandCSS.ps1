﻿$li = "<li>"
$liX = "</li>"
$ul = "<ul>"
$ulX = "</ul>"
$br = "<br/>"
$b = "<b>"
$bX = "</b>"
$green = '<font color="green">'
$red = '<font color="red">'
$fontX = "</font>"

$BeginFormat = @"
<!DOCTYPE html>
<head>
<title>Remote TroubleShooting Info</title>
<style>
body {
    font-family: Arial;
    color: #585858;
    background: #466368;
    background: linear-gradient(to bottom, #fff, #0F7CC3 90%, #0F7CC3);
    }

p {
    font-family: Arial;
    color: #585858;
    }

table {
    max-width: 1000px;
	border-collapse: collapse; 
	margin:50px auto;
	}

th { 
	background: #fdf6e3; 
	color: #0F7CC3; 
	font-weight: bold; 
	}

td, th { 
	padding: 10px; 
	border: 1px solid #ccc; 
	text-align: left; 
	font-size: 18px;
	}

h1 {
    text-align: center;
    color: #0F7CC3;
    }

ul {
    list-style: circle;
    padding-left: 1;
    }

.summary tr td {
	background-color: #fdf6e3;
	color: #839496;
	}

</style>

</head>
<body>

<div style="text-align: center;">
    <h1>REDACTED External Connectivity Test</h1>
    <p>Time of Tests: $($TimeOfTest)</p>
    <h2>For urgent issues (24hrs): REDACTED<br/>Toll free from Aus: REDACTED<br/>From an Australian office: ext 5000</h2>
    <p>The results in the third column will help IT Support get you working again</p>
</div>

<table>
    <thead>
        <tr>            
            <th style="text-align:center">Number</th>
            <th style="text-align:center">Test</th>
            <th style="text-align:center">Results</th>
            <th style="text-align:center">Remediation Steps</th>
        </tr>
    </thead>
<tbody>
"@

$SuccessFormat = @"
<tbody class='summary'>
    <tr>
        <td style="text-align:center">{0}</center></td>
        <td><font color="black">{1}</font></td>
        <td><font color="green">{2}</font></td>
        <td>{3}</td>
    </tr>
</tbody>
"@


$FailFormat = @"
<tbody class='summary'>
    <tr>
        <td style="text-align:center">{0}</center></td>
        <td><font color="black">{1}</font></td>
        <td><font color="red">{2}</font></td>
        <td>{3}</td>
    </tr>
</tbody>
"@

$EndFormat = @"
</tbody>
</table>
<p style="text-align:center">Version: $($Version)</p>
</body>
</html>
"@
