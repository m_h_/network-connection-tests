﻿Function getWLANs {
    $result = @{}
    $test = netsh wlan show networks     

    If (($test | Select-String -Pattern 'SSID' | Out-String) -eq "") {
        $result["Pass"] = $false
        $result["Result"] = $test[2].Trim()
    } Else {
        $result["Pass"] = $true 
        $result["Result"] = ($test | Select-String -Pattern 'SSID' | Out-String).Replace("SSID ", "").Trim().Replace("`n", "<br/>")  
    }

    Return $result
}