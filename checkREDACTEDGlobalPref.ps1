﻿Function checkREDACTEDGlobalPref {
    $result = @{}
    $preferences_globalLocation = "$env:Programdata\Cisco\REDACTED Client\preferences_global.xml"
    
    If([system.io.file]::Exists($preferences_globalLocation)) {
                
        # Get relevant line numbers
        $defaultHostNameLine = Select-String -Path $preferences_globalLocation -Pattern "<DefaultHostName>" | Select-Object -ExpandProperty LineNumber
        $defaultHostAddressLine = Select-String -Path $preferences_globalLocation -Pattern "<DefaultHostAddress>" | Select-Object -Expand LineNumber
        $enableAutomaticServerSelectionLine = Select-String -Path $preferences_globalLocation -Pattern "<EnableAutomaticServerSelection>" | Select-Object -Expand LineNumber
    
        # Get default host name
        $defaultHostName = ((Get-Content $preferences_globalLocation)[$defaultHostNameLine - 1]).Replace("<DefaultHostName>", "").Replace("</DefaultHostName>", "").Replace("Default Host", "")
        If ($defaultHostName.length -eq 0) {
            $preferences_globalMSG1 = "<font color='black'>Default Host Name: </font><font color='green'>NOT SET</font>"
        } Else {
            $preferences_globalMSG1 = "<font color='black'>Default Host Name: </font>" + $defaultHostName
        }
        
        # Get default host address
        $defaultHostAddress = ((Get-Content $preferences_globalLocation)[$defaultHostAddressLine - 1]).Replace("<DefaultHostAddress>", "").Replace("</DefaultHostAddress>", "").toUpper()
        
        If ($defaultHostAddress.length -eq 0) {
            $preferences_globalMSG2 = "<font color='black'>Default Host Address: </font><font color='green'>NOT SET</font>"
        } Else {
            $preferences_globalMSG2 = "<font color='black'>Default Host Address: </font>" + $defaultHostAddress
        }
        
        # Get enable automatic server selection setting
        $enableAutomaticServerSelection = ((Get-Content $preferences_globalLocation)[$enableAutomaticServerSelectionLine - 1]).Replace("<EnableAutomaticServerSelection>", "").Replace("</ControllablePreferences>", "").Replace("</EnableAutomaticServerSelection>", "").toUpper()
        
        If ($enableAutomaticServerSelection.length -eq 0) {
            $preferences_globalMSG3 = "<font color='black'>Auto Server Selection: </font><font color='red'>NOT SET</font>"
        } Else {
            $preferences_globalMSG3 = "<font color='black'>Auto Server Selection: </font>" + $enableAutomaticServerSelection
        }

        # Collect messages into one
        $result["Result"] = $preferences_globalMSG1 + $br + $preferences_globalMSG2 + $br + $preferences_globalMSG3
        $result["Pass"] = $true
    } Else {
        $result["Pass"] = $true
        $result["Result"] = "Preferences not altered"
    } 
    
    Return $result
}


