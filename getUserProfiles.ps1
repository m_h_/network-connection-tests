﻿Function getUserProfiles {
    $result = @{}

    $test = Netsh wlan show profiles
    
    $userProfiles = ($test | Select-String "All User Profile" | Out-String).Replace("All User Profile", "").Replace(":", "").Replace(" ", "").Trim().Replace("`n","<br/>")

    If($test -Match "The Wireless AutoConfig Service*"){ # When Wifi is off, full results will display
        $result["Pass"] = $false
        $result["Result"] = $test.Trim()

    } Else {
        $profiles = ""
        ForEach($profile in $userProfiles) {
            $profiles += $profile + "<br/>"
        }
        
        $result["Pass"] = $true
        $result["Result"] = $profiles
    }
    return $result
}

