﻿Function DNSLookup($uri) {
    $result = @{}
        
    $test = nslookup $uri 

    $sdrvpnIP = "REDACTED"
    $sdcvpnIP = "REDACTED"
    $sdrvpnIPExists = $test | Select-String $sdrvpnIP | Out-String
    $sdcvpnIPExists = $test | Select-String $sdcvpnIP | Out-String

    $server = ($test | Select-String "Server:" | Out-String).Replace(" ", "").Trim()
    $address = ($test | Select-String "Address:" | Out-String).Replace(" ", "").Trim()
    $name = ($test | Select-String "Name:" | Out-String).Replace(" ","").Trim()
    $addresses = ($test | Select-String "^\s+\d+.\d+.\d+.\d+$" | Out-String).Trim()
    
    $result["Pass"] = $false   
    $result["Result"] = "NSlookup failed: " + $uri

    If($sdrvpnIPExists -Or $sdcvpnIPExists) {
        $result["Pass"] = $true
        $result["Result"] = "Name server resolved successfully<br/>" + $server + "<br/>" `
        + $address + "<br/>" + $name + "<br/>" + "Addresses: " + $addresses
    }
    
    Return $result
}

# example DNSLookup("REDACTED.com.au")