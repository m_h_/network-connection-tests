﻿Function resolveDNS($url) {
    $result = @{}
    $test = Resolve-DnsName $url | Where-Object { $_.Type -eq "A"} | Select-Object -ExpandProperty IPAddress
      
    If ($test) {
        $result["Pass"] = $true
        $result["IP"] = $test
        $result["Result"] = "Successfully resolved " + $url
    } Else {
        $result["Pass"] = $false
        $result["Result"] = $error[0].Exception | Out-String
    }
    Return $result
}

# resolveDNS("www.google.com")