﻿Function getMyIP {
    $result = @{}

    $test = Get-WmiObject Win32_NetworkAdapterConfiguration | Where-Object { $_.IPAddress -ne $null -and $_.DefaultIPGateway -ne $null }
    $IPAddress = ($test | Select-Object -ExpandProperty IPAddress)[0]
    $defaultGateway = ($test | Select-Object -ExpandProperty DefaultIPGateway)

    $result["Pass"] = $false
    $result["Result"] = "No service has both an IP and a Default IP Gateway set"

    If ($IPAddress) {
        If($IPAddress -like "169.*") {
            $result["Result"] = "DHCP issue. IP: " + $IPAddress
            $result["Pass"] = $false
        } Else {
            If($defaultGateway -eq "") {
                $result["Result"] = "<font color='black'>Default IP Gateway: </font><font color='red'>NOT SET</font>"
                $result["Pass"] = $false
            }
        }

        $result["Result"] = "<font color='black'>IP: </font><font color='green'>" + $IPAddress + $br + "</font><font color='black'>Default Gateway: </font><font color='green'>" + $defaultGateway + "</font>"
        $result["Pass"] = $true
    }

    Return $result
}
