﻿#Not sure which the people want... so here is both
# Turns autodetect off


Function CheckWebProxy {
    $result = @{}
    $registryPath = "Registry::HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    $test = Get-ItemProperty -Path $registryPath
    $result["Pass"] = $true
    $result["Result"] = "Settings are set to default"
    $remediation += ""
    $resultMessage = ""

    # Checks AutoDetect setting in IE>Internet Options>Connections>LAN settings
    # We want this off
    If($test.AutoDetect -ne $null) { #If AutoDetect exists
        If($test.AutoDetect -eq 0) { #If AutoDetect set to off        
            $resultMessage += "<font color='black'>Automatically detect settings:</font><font color='green'> No</font><br /><br />"
        } Else { # If AutoDetect set on
            $result["Pass"] = $false
            $resultMessage += "<font color='black'>Automatically detect settings: </font><font color='red'> Yes</font><br /><br />"
            $remediation += "Open Internet Explorer. Click the Cog button top right. Go to Internet Options>Connections>LAN settings. Untick 'Automatically detect settings' Click 'OK' twice until out of Internet Options<br /><br />"
        }
    } Else { # AutoDetect registry entry does not exist this is okay
        $resultMessage += "<font color='black'>Automatically detect settings: </font><font color='green'> No</font><br /><br />"
    }

    #Sets AutoConfigURL setting Off in IE>Internet Options>Connections>LAN settings
    # We want this off
    If($test.AutoConfigURL -ne $null) { #AutoConfigURL exists
        If($test.AutoConfigURL -eq 0) { #AutoConfigURL set to off        
            $resultMessage += $result["Result"] + "<font color='black'>Use automatic configuration script:</font><font color='green'> No</font><br /><br />"
        } Else { #AutoConfigURL set on
            $result["Pass"] = $false
            $resultMessage = "<font color='black'>Use automatic configuration script: </font><font color='red'> Yes</font><br /><br />"
            $remediation += "Open Internet Explorer. Click the Cog button top right. Go to Internet Options>Connections>LAN settings. Untick 'Use automatic configuration script' Click 'OK' twice until out of Internet Options<br /><br />"
        }
    }

    #Sets ProxyEnable setting Off in IE>Internet Options>Connections>LAN settings
    # We want this off
    If($test.ProxyEnable -ne $null) { #ProxyEnable exists
        If($test.ProxyEnable -eq 0) { #ProxyEnable set to off       
            $result["Result"] = $result["Result"] + "<font color='black'>Use a proxy server for your LAN:</font><font color='green'> No</font>"
        } Else { #ProxyEnable set on
            $result["Pass"] = $false
            $resultMessage += "<font color='black'>Use a proxy server for your LAN: </font><font color='red'> Yes</font>"
            $remediation += "Open Internet Explorer. Click the Cog button top right. Go to Internet Options>Connections>LAN settings. Untick 'Use a proxy server for your LAN...' Click 'OK' twice until out of Internet Options"
        }
    }
    
    $result["Remediation"] = $remediation
    $result["Result"] = $resultMessage
    Return $result
}


# Turns autodetect on
<#
Function CheckWebProxy {
    $result = @{}
    $registryPath = "Registry::HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    $test = Get-ItemProperty -Path $registryPath
    $result["Pass"] = $true
    $result["Result"] = "Settings not changed from default"
    $remediation += ""
    $resultMessage = ""

    #Sets AutoDetect setting On in IE>Internet Options>Connections>LAN settings
    # We want this on
    $name = "AutoDetect"
    $value = "1"
    If($test.AutoDetect -ne $null) { #AutoDetect exists
        If($test.AutoDetect -eq 1) { #AutoDetect set to On        
            $resultMessage += "<font color='black'>Automatically detect settings:</font><font color='green'> Yes</font><br /><br />"
        } Else { #AutoDetect set off
            New-ItemProperty -Path $registryPath -Name $name -Value $value -PropertyType DWORD -Force | Out-Null
            $resultMessage += "<font color='black'>Automatically detect settings: </font><font color='green'> Yes</font><br /><br />"
        }
    } Else { # AutoDetect registry entry does not exist, it is created
        New-ItemProperty -Path $registryPath -Name $name -Value $value -PropertyType DWORD -Force | Out-Null
        $resultMessage += "<font color='black'>Automatically detect settings: </font><font color='green'> Yes</font><br /><br />"
    }

    #Sets AutoConfigURL setting Off in IE>Internet Options>Connections>LAN settings
    # We want this off
    If($test.AutoConfigURL -ne $null) { #AutoConfigURL exists
        If($test.AutoConfigURL -eq 0) { #AutoConfigURL set to off        
            $resultMessage += $result["Result"] + "<font color='black'>Use automatic configuration script:</font><font color='green'> No</font><br /><br />"
        } Else { #AutoConfigURL set on
            $result["Pass"] = $false
            $resultMessage = "<font color='black'>Use automatic configuration script: </font><font color='red'> Yes</font><br /><br />"
            $remediation += "Open Internet Explorer. Click the Cog button top right. Go to Internet Options>Connections>LAN settings. Untick 'Use automatic configuration script' Click 'OK' twice until out of Internet Options<br /><br />"
        }
    }

    #Sets ProxyEnable setting Off in IE>Internet Options>Connections>LAN settings
    # We want this off
    If($test.ProxyEnable -ne $null) { #ProxyEnable exists
        If($test.ProxyEnable -eq 0) { #ProxyEnable set to off       
            $result["Result"] = $result["Result"] + "<font color='black'>Use a proxy server for your LAN:</font><font color='green'> No</font>"
        } Else { #ProxyEnable set on
            $result["Pass"] = $false
            $resultMessage += "<font color='black'>Use a proxy server for your LAN: </font><font color='red'> Yes</font>"
            $remediation += "Open Internet Explorer. Click the Cog button top right. Go to Internet Options>Connections>LAN settings. Untick 'Use a proxy server for your LAN...' Click 'OK' twice until out of Internet Options"
        }
    }
    
    $result["Remediation"] = $remediation
    $result["Result"] = $resultMessage
    Return $result
}
#>