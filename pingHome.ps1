﻿Function pingHome {
    $result = @{}
    $test = Test-NetConnection 127.0.0.1

    If($test.PingSucceeded -eq "True") {
        $result["Pass"] = $true
        $result["Result"] = "Successfully reached 127.0.0.1"
        $result["Remediation"] = ""
    } Else {
        $result["Pass"] = $false
        $result["Result"] = "'Ping 127.0.0.1' failed"
        $result["Remediation"] = "Faulty Ethernet/WiFi network adaptor. You require a laptop replacement."
    }

    Return $result
}