﻿Function REDACTED {
    $result = @{}
    $test = Get-Process -Name "REDACTED"
    $result["Pass"] = $false

    If($test) {
        $result["Pass"] = $true
        $result["Result"] = "Application Running"
    } Else {
        & "C:\Program Files (x86)\REDACTED\REDACTED REDACTED Client\REDACTED.exe"
    
        $test = Get-Process -Name "REDACTED"

        If(!$test) {
            $result["Result"] = "Could not start"
        } Else {
            $result["Pass"] = $true
            $result["Result"] = "It has been started and is now running"
        }
    }

    Return $result
}