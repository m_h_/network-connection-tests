﻿Function checkREDACTEDUserPref {
    $result = @{}
    $preferences_userLocation = "$env:LOCALAPPDATA\Cisco\Cisco REDACTED REDACTED\preferences.xml"
    
    If([system.io.file]::Exists($preferences_userLocation)) { 

        # Get relevant line numbers
        $defaultHostNameLine = Select-String -Path $preferences_userLocation -Pattern "<DefaultHostName>" | Select-Object -ExpandProperty LineNumber       
        $defaultHostAddressLine = Select-String -Path $preferences_userLocation -Pattern "<DefaultHostAddress>" | Select-Object -Expand LineNumber       
        $enableAutomaticServerSelectionLine = Select-String -Path $preferences_userLocation -Pattern "<EnableAutomaticServerSelection>" | Select-Object -Expand LineNumber

        # Get settings
        $enableAutomaticServerSelection = (((Get-Content $preferences_userLocation)[$enableAutomaticServerSelectionLine - 1]).Replace("<EnableAutomaticServerSelection>", "").Replace("</ControllablePreferences>", "").Replace("</EnableAutomaticServerSelection>", "")).toUpper()
        $defaultHostName = ((Get-Content $preferences_userLocation)[$defaultHostNameLine - 1]).Replace("<DefaultHostName>", "").Replace("</DefaultHostName>", "")        
        $defaultHostAddress = ((Get-Content $preferences_userLocation)[$defaultHostAddressLine - 1]).Replace("<DefaultHostAddress>", "").Replace("</DefaultHostAddress>", "").toUpper()        
        
        If ($enableAutomaticServerSelection.length -eq 0) {
            $preferences_userMSG3 = "<font color='black'>Auto Server Selection: </font><font color='green'>NOT SET</font>"
        } Else {
            $preferences_userMSG3 = "<font color='black'>Auto Server Selection: </font>" + $enableAutomaticServerSelection
        }
       
        If ($defaultHostName.length -eq 0) {
            $preferences_userMSG1 = "<font color='black'>Default Host Name: </font><font color='green'>NOT SET</font>"
        } Else {
            $preferences_userMSG1 = "<font color='black'>Default Host Name: </font>" + $defaultHostName
            
        }
        
        If ($defaultHostAddress.length -eq 0) {
            $preferences_userMSG2 = "<font color='black'>Default Host Address: </font><font color='green'>NOT SET</font>"
        } Else {
            $preferences_userMSG2 = "<font color='black'>Default Host Address: </font>" + $defaultHostAddress
        }
         
        If($enableAutomaticServerSelection -eq "true") {
            $result["Pass"] = $true
        } Else {
            If ($defaultHostName -eq "AllensAUS") {
                $result["Pass"] = $true
            } Else {
                $result["Pass"] = $false
            }
        }

        # Collect messages into one
        $result["Result"] = $preferences_userMSG1 + $br + $preferences_userMSG2 + $br + $preferences_userMSG3

    } Else {
        $result["Pass"] = $true # It is okay if this file does not exist, it means that the user has made no changes to default settings
        $result["Result"] = "File Not Found"
    } 

    Return $result
}


