﻿Function captivePortal ($site) {
    $result = @{}       
   
    Try {
        $test = Invoke-WebRequest -Uri $site -method get -MaximumRedirection 0
    } Catch {
        $failure = $_.Exception.Response     
    }
 
    If($failure) {
        $result["Pass"] = $false
        $result["Result"] = $failure.StatusDescription

        If($failure.Server -eq "WebMarshal Proxy") {
            $result["Pass"] = $true
            $result["Result"] = $failure.StatusDescription +  "<br/>Test blocked by 'REDACTED', captive portal not confirmed or denied" 
            $result["Remediation"] = "If you are on the REDACTED network, you can ignore this result."
        } Else {
            $result["Remediation"] = ""
        }
    } Else {
        If($test.Content -Match "Success") {
            $result["Pass"] = $true
            $result["Result"] = "Captive Portal Not Detected"
            $result["Remediation"] = ""
        } Else {
            $result["Pass"] = $false
            $result["Result"] = "Captive Portal Detected"
            $result["Remediation"] = "" #what is the solution to a captive portal?
        }
    }

    Return $result
}

# captivePortal("http://captive.apple.com")
