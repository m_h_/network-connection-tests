﻿<#
.SYNOPSIS
    Runs a series of tests to diagnose networking issues on a local machine
    Intended to troubleshooting clients when away from office and not on the
    REDACTED network. The results can be relayed to IT Helpdesk over the phone.
    Supporting documentation for IT available at confluence:
    REDACTED

.DESCRIPTION
    Runs a series of tests and outputs the results by opening the default browser 
    on the system and displaying a basic HTML page with a table of results
    The file that is generated is located at 
    C:\Users\<USERNAME>\External_Connectivity_Test.html – where <USERNAME> is staffcode
    
.CONFLUENCE
    REDACTED
    
.INPUTS
    None

.OUTPUTS
    A html file, launched in default browser

.NOTES
    Version: 1.5
    Author: Matt Hanson
    Creation Date: 11 Jan 2018
    Purpose/Change: minor fixes
    
    Version: 1.0
    Author: Matt Hanson
    Creation Date: 14 Sept 2018
    Purpose/Change: Initial verison

.EXAMPLE
    Deployed via SCCM
    Start menu "REDACTED External Connectivity Test"
#>

# Pre-tests setup
$Version = "1.5" # Used in the output at the bottom of the webpage
$TimeOfTest =  Get-Date -format "dd-MMM-yyyy-HH-mm-ss"  # Used in filename
$sb = New-Object System.Text.StringBuilder
. "$PSScriptRoot\HTMLandCSS.ps1" # Imports the html and css code for the string builder
$sb.Append($BeginFormat) | Out-Null # String builder creation
$testNumber = 1 # Counter for Test Number column

Write-Host "REDACTED External Connectivity Test"
Write-Host "`nResults will save to $env:USERPROFILE\External_Connectivity_Test-$($TimeOfTest).html`n"
Write-Host "24 Tests running..."

$ErrorActionPreference = "SilentlyContinue" # Supresses popup messages for ping tests
$continue = $true # Loop control

# Main test loop
While ($continue -eq $true) {

    # 1 checkHowConnected
    Write-Host $testNumber ": Checking how you are connected"
    . "$PSScriptRoot\checkHowConnected.ps1" 
    $checkHowConnectedTest = checkHowConnected
    $testNumber+=1
    If($checkHowConnectedTest["Pass"]){
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "Connection Detail", $checkHowConnectedTest["Result"],`
          $checkHowConnectedTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1), "Connection Detail",`
         $checkHowConnectedTest["Result"],`
         $checkHowConnectedTest["Remediation"])) | Out-Null
    }
    
    # 2 pingHomeTest
    Write-Host $testNumber ": Checking for wifi hardware issues"
    . "$PSScriptRoot\pingHome.ps1" 
    $pingHomeTest = pingHome  
        $testNumber+=1
    If($pingHomeTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "Wifi hardware check", $pingHomeTest["Result"],`
          $pingHomeTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "Wifi hardware check", $pingHomeTest["Result"],`
          $pingHomeTest["Remediation"])) | Out-Null
    }

    
    # 3 checkLANconnection
    Write-Host $testNumber ": Checking ethernet cable"
    . "$PSScriptRoot\checkLANconnection.ps1" 
    $checkLANconnectionTest = checkLANconnection   
    $testNumber += 1 
    If($checkLANconnectionTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "Ethernet cable check", $checkLANconnectionTest["Result"],`
          $checkLANconnectionTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "Ethernet cable check", $checkLANconnectionTest["Result"],`
          $checkLANconnectionTest["Remediation"])) | Out-Null
    }

    
    # 4 getWifiInfo
    Write-Host $testNumber ": Getting wifi connection details"
    . "$PSScriptRoot\forceWifiRescan.ps1" 
    forceWifiRescan # forces a rescan of networks so that netsh doesn't rely on cached results
    Sleep -Second 4
    . "$PSScriptRoot\getWifiInfo.ps1" 
    $getWifiInfoTest = getWifiInfo
    $testNumber += 1
    If($getWifiInfoTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "Wifi Info", $getWifiInfoTest["Result"],`
          $getWifiInfoTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "Wifi Info", $getWifiInfoTest["Result"],`
          $getWifiInfoTest["Remediation"])) | Out-Null
    }

    
    # 5 checkWifiDevice
    Write-Host $testNumber ": Checking if WiFi is enabled"
    . "$PSScriptRoot\checkWifiDevice.ps1" 
    $checkWifiDeviceTest = checkWifiDevice
    $testNumber += 1
    If($checkWifiDeviceTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "Wifi Enabled check", $checkWifiDeviceTest["Result"],`
          $checkWifiDeviceTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "Wifi Enabled check", $checkWifiDeviceTest["Result"],`
          $checkWifiDeviceTest["Remediation"])) | Out-Null
    }
    

    # 6 getNetworkName
    Write-Host $testNumber ": Getting WiFi network name"
    . "$PSScriptRoot\getNetworkName.ps1" 
    $networkNameTest = getNetworkName
    $testNumber += 1
    If($networkNameTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1), `
          "Network Name", $networkNameTest["Result"],`
          $networkNameTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
          "Network Name", $networkNameTest["Result"],`
          $networkNameTest["Remediation"])) | Out-Null
    }
    
        
    # 7 getWLANs
    Write-Host $testNumber ": Scanning WIFI networks"
    forceWifiRescan # Forces a rescan of networks so that netsh doesn't rely on cached results
    Sleep -Second 4
    . "$PSScriptRoot\getWLANs.ps1" 
    $WLANsTest = getWLANs
    $testNumber += 1
    If($WLANsTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "WiFi Networks", $WLANsTest["Result"],`
          $WLANsTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "WiFi Networks", $WLANsTest["Result"],`
          $WLANsTest["Remediation"])) | Out-Null
    }

    
    # 8 getUserProfiles 
    Write-Host $testNumber ": Getting network profiles"
    . "$PSScriptRoot\getUserProfiles.ps1" 
    $getUserProfilesTest = getUserProfiles
    $testNumber += 1
    If($getUserProfilesTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "Network Profiles", $getUserProfilesTest["Result"],`
          $getUserProfilesTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "Network Profiles", $getUserProfilesTest["Result"],`
          $getUserProfilesTest["Remediation"])) | Out-Null
    }


    # 9 getMyIP
    Write-Host $testNumber ": Getting your IP address"
    . "$PSScriptRoot\getMyIP.ps1" 
    $getMyIPTest = getMyIP
    $testNumber += 1
    if($getMyIPTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "IP address", $getMyIPTest["Result"],`
          $getMyIPTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "IP address", $getMyIPTest["Result"],`
          $getMyIPTest["Remediation"])) | Out-Null
    }
    
    
    # 10 getDHCPServer
    Write-Host $testNumber ": Getting DHCP Server IP address"
    . "$PSScriptRoot\getDHCPServer.ps1" 
    $getDHCPServerTest = getDHCPServer
    $testNumber += 1
    If($getDHCPServerTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "DHCP Server Ip Address", $getDHCPServerTest["Result"],`
          $getDHCPServerTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "DHCP Server Ip Address", $getDHCPServerTest["Result"],`
          $getDHCPServerTest["Remediation"])) | Out-Null
    }
   

    # 11 pingLocationTestsdcvpn
    Write-Host $testNumber ": Checking connection to REDACTED.REDACTED.com.au"
    . "$PSScriptRoot\pingLocation.ps1" 
    $pingLocationTestsdcvpn = pingLocation("REDACTED.REDACTED.com.au")
    $testNumber += 1
    If($pingLocationTestsdcvpn["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "Connection to REDACTED.REDACTED.com.au", $pingLocationTestsdcvpn["Result"],`
          $pingLocationTestsdcvpn["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "Connection to REDACTED.REDACTED.com.au", $pingLocationTestsdcvpn["Result"],`
          $pingLocationTestsdcvpn["Remediation"])) | Out-Null
    }


    # 12 pingLocationTestsdcvpn
    Write-Host $testNumber ": Checking connection to REDACTED.REDACTED.com.au"
    $pingLocationTestsdrvpn = pingLocation("REDACTED.REDACTED.com.au")
    $testNumber += 1
    If($pingLocationTestsdrvpn["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "Connection to REDACTED.REDACTED.com.au", $pingLocationTestsdrvpn["Result"],`
          $pingLocationTestsdrvpn["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "Connection to REDACTED.REDACTED.com.au", $pingLocationTestsdrvpn["Result"],`
          $pingLocationTestsdrvpn["Remediation"])) | Out-Null
    }
    

    # 13 pingLocationTestgoogle
    Write-Host $testNumber ": Checking connection to google.com"
    $pingLocationTestgoogle = pingLocation("google.com")
    $testNumber += 1
    If($pingLocationTestgoogle["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "Connection to google.com", $pingLocationTestgoogle["Result"],`
          $pingLocationTestgoogle["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "Connection to google.com", $pingLocationTestgoogle["Result"],`
          $pingLocationTestgoogle["Remediation"])) | Out-Null
    }


    # 14 pingLocationTestREDACTED
    Write-Host $testNumber ": Checking connection to REDACTED.com.au"
    $pingLocationTestREDACTED = pingLocation("www.REDACTED.com.au")
    $testNumber += 1
    If($pingLocationTestREDACTED["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "Connection to REDACTED.com.au", $pingLocationTestREDACTED["Result"],`
          $pingLocationTestREDACTED["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "Connection to REDACTED.com.au", $pingLocationTestREDACTED["Result"],`
          $pingLocationTestREDACTED["Remediation"])) | Out-Null
    }
    

    # 15 DNSLookup 
    Write-Host $testNumber ": Checking for DNS -- REDACTED"
    . "$PSScriptRoot\DNSLookup.ps1" 
    $DNSLookupTestsdr = DNSLookup("REDACTED.REDACTED.com.au")
    $testNumber += 1
    If($DNSLookupTestsdr["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "DNS status -- REDACTED", $DNSLookupTestsdr["Result"],`
          $DNSLookupTestsdr["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "DNS status -- REDACTED", $DNSLookupTestsdr["Result"],`
          $DNSLookupTestsdr["Remediation"])) | Out-Null
    }


     # 16 DNSLookup 
    Write-Host $testNumber ": Checking for DNS -- REDACTED"
    . "$PSScriptRoot\DNSLookup.ps1" 
    $DNSLookupTestsdc = DNSLookup("REDACTED.REDACTED.com.au")
    $testNumber += 1
    If($DNSLookupTestsdc["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "DNS status -- REDACTED", $DNSLookupTestsdc["Result"],`
          $DNSLookupTestsdc["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "DNS status -- REDACTED", $DNSLookupTestsdc["Result"],`
          $DNSLookupTestsdc["Remediation"])) | Out-Null
    }

    
    # 17 getHTTPCode
    Write-Host $testNumber ": Attempting HTTP access"
    . "$PSScriptRoot\getHTTPCode.ps1"    
    $getHTTPCodeTest = getHTTPCode("http://www.google.com.au")
    $testNumber += 1
    If($getHTTPCodeTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "HTTP test", $getHTTPCodeTest["Result"],`
          $getHTTPCodeTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "HTTP test", $getHTTPCodeTest["Result"],`
          $getHTTPCodeTest["Remediation"])) | Out-Null
    }
    
    
    # 18 captivePortal
    Write-Host $testNumber ": Checking for a captive portal"
    . "$PSScriptRoot\captivePortal.ps1" 
    $captivePortalTest = captivePortal("http://captive.apple.com")
    $testNumber += 1
    If($captivePortalTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "Captive portal", $captivePortalTest["Result"],`
          $captivePortalTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "Captive portal", $captivePortalTest["Result"],`
          $captivePortalTest["Remediation"])) | Out-Null
    }

    
    # 19 VPNui
    Write-Host $testNumber ": Checking REDACTED is running"
    . "$PSScriptRoot\VPNui.ps1" 
    $VPNuiTest = VPNui
    $testNumber += 1
    If($VPNuiTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "REDACTED", $VPNuiTest["Result"],`
          $VPNuiTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "REDACTED", $VPNuiTest["Result"],`
          $VPNuiTest["Remediation"])) | Out-Null
    }
    

    # 20 checkREDACTEDConnect
    Write-Host $testNumber ": Checking REDACTED is running"
    . "$PSScriptRoot\checkREDACTEDConnect.ps1" 
    $checkREDACTEDConnectTest = checkREDACTEDConnect
    $testNumber += 1
    If($checkREDACTEDREDACTEDTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "REDACTED", $checkREDACTEDREDACTEDTest["Result"],`
          $checkREDACTEDREDACTEDTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "REDACTED", $checkREDACTEDREDACTEDTest["Result"],`
          $checkREDACTEDREDACTEDTest["Remediation"])) | Out-Null
    }
    

    # 21 checkREDACTEDGlobalPref
    Write-Host $testNumber ": Checking REDACTED global preferences"
    . "$PSScriptRoot\checkREDACTEDGlobalPref.ps1" 
    $checkREDACTEDGlobalPrefTest = checkREDACTEDGlobalPref
    $testNumber += 1
    If($checkREDACTEDGlobalPrefTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "REDACTED REDACTED Global Preferences", $checkREDACTEDGlobalPrefTest["Result"],`
          $checkREDACTEDGlobalPrefTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "REDACTED REDACTED Global Preferences", $checkREDACTEDGlobalPrefTest["Result"],`
          $checkREDACTEDGlobalPrefTest["Remediation"])) | Out-Null
    }
    

    # 22 checkREDACTEDUserPref
    Write-Host $testNumber ": Checking REDACTED local preferences"
    $checkREDACTEDUserPrefTest = checkREDACTEDUserPref
    $testNumber += 1
    If($checkREDACTEDUserPrefTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "REDACTED REDACTED local Preferences", $checkREDACTEDUserPrefTest["Result"],`
          $checkREDACTEDUserPrefTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "REDACTED REDACTED local Preferences", $checkREDACTEDUserPrefTest["Result"],`
          $checkREDACTEDUserPrefTest["Remediation"])) | Out-Null
    }


    # 23 CheckWebProxy
    Write-Host $testNumber ": Checking Internet Explorer Settings"
    . "$PSScriptRoot\CheckWebProxy.ps1" 
    $CheckWebProxyTest = CheckWebProxy
    $testNumber += 1
    If($CheckWebProxyTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "Internet Explorer", $CheckWebProxyTest["Result"],`
         $CheckWebProxyTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "Internet Explorer",  $CheckWebProxyTest["Result"],`
         $CheckWebProxyTest["Remediation"])) | Out-Null
    }


    # 24 CheckAccountAndPassword
    Write-Host $testNumber ": Checking Account and Password Expiry Status"
    . "$PSScriptRoot\checkAccountAndPassword.ps1" 
    $checkAccountAndPasswordTest = checkAccountAndPassword
    $testNumber += 1
    If($checkAccountAndPasswordTest["Pass"]) {
        $sb.Append(($SuccessFormat -f ($testNumber-1),`
         "Account and Password Status", $checkAccountAndPasswordTest["Result"],`
         $checkAccountAndPasswordTest["Remediation"])) | Out-Null
    } Else {
        $sb.Append(($FailFormat -f ($testNumber-1),`
         "Account and Password Status", $checkAccountAndPasswordTest["Result"],`
         $checkAccountAndPasswordTest["Remediation"])) | Out-Null
    }


    $continue = $false
}

# Finalising
$sb.Append($EndFormat) | Out-Null
Write-Host "Finishing up..."

# Set location and filename of output HTML file
$location = "$env:USERPROFILE\External_Connectivity_Test" # Set output file location
#$a = (Get-Date).ToString('-dd-MMM-yyyy_HH-mm-ss')
$filename = $location + $TimeOfTest + ".html"
$sb.ToString() | Out-File $filename # Create output file

# Gets default browser on local system
Function GET-DefaultBrowserPath {
    New-PSDrive -Name HKCR -PSProvider registry -Root Hkey_Classes_Root | Out-Null    
    $browserPath = ((Get-ItemProperty 'HKCR:\http\shell\open\command').'(default)').Split('"')[1]    
    return $browserPath
}

$defaultBrowser = Get-DefaultBrowserPath
Start-Process $defaultBrowser $filename # Open output file
$ErrorActionPreference = "Continue" # Reset global preference
