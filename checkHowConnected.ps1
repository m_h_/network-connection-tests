﻿Function checkHowConnected {

    $result = @{}
    $test = Test-Connection REDACTED.com.au
    
    # Default result
    $result["Result"] = "Not connected to REDACTED network"
    $result["Pass"] = $false 
    
    If($test) {
        $y = $test.IPV4Address[0].IPAddressToString
        
        If($y -eq "REDACTED") {
            $result["Pass"] = $true 
            $result["Result"] = "Connected to REDACTED internal network"
        } ElseIf($y -eq "REDACTED" -Or $y -eq "REDACTED") {
            $result["Pass"] = $true 
            $result["Result"] = "Connected to REDACTED via VPN"
        }
    }

    Return $result
}
