﻿Function getWifiInfo {
    $result = @{}

    $test = netsh wlan show interfaces 

    If($test -Match "The Wireless AutoConfig Service (wlansvc) is not running.") {
        $result["Pass"] = $false    
        $result["Result"] = "The Wireless AutoConfig Service (wlansvc) is not running."
        $result["Remediation"] = "Please start the 'wlansvc' service"

    } ElseIf(($test | Select-String "State") -Like "*connected*" -And ($test | Select-String "State") -NotLike "*disconnected*") { #true if state == connected
        $BSSID = $test | Select-String BSSID | Out-String
        $SSIDandBSSID = $test | Select-String SSID | Out-String
        $SSID = ($SSIDandBSSID -replace $BSSID, "").Replace("SSID","").Replace(":","").Trim()

        $ConnectionMode = $test | Select-String "Connection mode"
        $ConnectionModeAndProfile = $test | Select-String Profile
        $Profile = ($ConnectionModeAndProfile -Replace $ConnectionMode, "").Replace("Profile","").Replace(":","").Trim()

        $strength = ($test | Select-String Signal | Out-String).Replace("Signal", "").Replace(":","").Trim()

        $result["Pass"] = $true
        $result["Result"] = "<font color='black'>State: </font><font color='green'>Connected</font><br/><font color='black'>Profile: </font>" `
            + $Profile + "<br/><font color='black'>SSID: </font>" + $SSID + "<br/><font color='black'>Signal Strength: </font>" + $strength
        $result["Remediation"] = ""

    } ElseIf(($test | Select-String "State") -Like "*disconnected*") {#true if state == disconnected
        $result["Pass"] = $false
        $hardwareStatus = ($test | Select-String "Hardware" | Out-String).Replace("Radio status",""). Replace(":","").Trim()
        $softwareStatus = ($test | Select-String "Software" | Out-String).Trim()
        $result["Result"] = "<font color='black'>State: </font><font color='red'>Disconnected</font><br/><font color='black'>Radio status: </font>" `
            + $hardwareStatus + ", " + $softwareStatus
        $result["Remediation"] = "Turn on wifi or install a wifi adapter"

    } ElseIf (($test | Select-String "State") -Like "*disabled*") { #true if state == disabled
        Enable-NetAdapter -Name Wi-Fi -Confirm:$false    
        $result["Pass"] = $false
        $result["Result"] = $test
        $result["Remediation"] = "Please check wifi is enabled or enable it now"    
    } Else {
        $result["Pass"] = $false
        $result["Result"] = $test
        $result["Remediation"] = "Is your iPhone plugged in and on HotSpot? If so, turn off hotspot and/or disconnect the iPhone."
    }
    Return $result
}