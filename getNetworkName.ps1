﻿Function getNetworkName {
    $result = @{}
    $test = Get-NetConnectionProfile

    If($test) {
        If($test.GetType().IsArray) {
            $result["Pass"] = $true
            
            ForEach($profile in $test) {
                $result["Result"] += $profile.InterfaceAlias + ": " + $profile.Name + "; "
        }
    } ElseIf ($test.getType() -Is [Object]) {
        $result["Pass"] = $true
        $result["Result"] = $test | Select -ExpandProperty Name
        }     
    } Else {
        $result["Pass"] = $false
        $result["Result"] = "No Network Name"
        $result["Remediation"] = "Connect to a network. Try clicking the networking/wifi logo in the system tray, and choosing a known secure network, and click connect. Enter the password to your network if necessary."
    }
 
    Return $result
}
