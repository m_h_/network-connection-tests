﻿Function getHTTPCode ($testUrl) {
    $result = @{}

    $WebRequest = ([System.Net.WebRequest]::GetSystemWebproxy()).GetProxy($testUrl)
    $test = Invoke-WebRequest $testUrl -Proxy $WebRequest -ProxyUseDefaultCredentials
    
    $result["Result"] = "<font color='black'>HTTP Status Code: </font>" `
                        + [string]$test.StatusCode + "<br /><font color='black'>Code Description: </font>" `
                        + $test.StatusDescription    
    $result["Remediation"] = ""
        
    If($test.StatusCode -eq 200) {
        $result["Pass"] = $true        
    } ElseIf($test.StatusCode -eq 407) {
        $result["Pass"] = $false
        $result["Remediation"] = "You may have been locked out of your account. Please log off from your computer, and call the IT Helpdesk to have them unlock your account."
    } Else {
        $result["Pass"] = $false
        $result["Result"] = $error[0].Exception | Out-String
        $result["Remediation"] = "Please read the results to your IT helpdesk team member"
    }
                
    return $result
}

# getHTTPCode("http://www.google.com")