﻿Function checkWifiDevice {
    $result = @{}
    $Wifi = Get-NetAdapter -Physical | Where-Object {$_.PhysicalMediaType -eq "Native 802.11"}
 
    If($Wifi.Status -eq "Up") {
        $result["Pass"] = $true
        $result["Result"] = "<font color='black'>Device: </font>" + $Wifi.InterfaceDescription + "<br/><font color='black'>Status: </font>" + $Wifi.Status 
        $result["Remediation"] = ""
    } Else {
        Enable-NetAdapter -Name "Wi-Fi" -Confirm:$false
        $Wifi2 = Get-NetAdapter -Physical | Where-Object {$_.PhysicalMediaType -eq "Native 802.11"}

        If($Wifi2.Status -eq "Up") {
            $result["Pass"] = $true
            $result["Result"] = "Adapter has been turned on.<br/><font color='black'>Device: </font>" + $Wifi.InterfaceDescription + "<br/><font color='black'>Status: </font>" + $Wifi.Status
            $result["Remediation"] = "" 
        } Else {            
            #netsh wlan connect
            $result["Pass"] = $false
            $result["Result"] = "Failed to start the Wifi adapter"
            $result["Remediation"] = "Click on the Wifi icon in the system tray (bottom right of task bar), and turn on Wi-Fi, which is at the bottom left. The Wi-Fi button should indicate it is on."
        }
    }
 
    Return $result
}
