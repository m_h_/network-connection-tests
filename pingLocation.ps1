﻿# pingLocation -- Test locations provided at parametre as array
Function pingLocation ($target) {
    $result = @{}    

    $test0 = Resolve-DnsName $target | Where-Object { $_.Type -eq "A"} | Select-Object -ExpandProperty IPAddress
      
    If ($test0) {
        $test = Ping $target
        
        If($test -match "Lost = [0-9]+") { # match any result        
            $result["Pass"] = $true # set default result
            $fontColour = "<font color='green'>"
            $testX = $test | Out-String
            $location = $testX.IndexOf("% loss")

            If($testX -match "\(\d% loss") # match single digit loss
            {
                $startLocation = $location - 1
                $loss = $testX.SubString($startLocation, 1)

                If($loss -ne "0")
                { 
                    $result["Pass"] = $false 
                    $fontColour = "<font color='red'>"
                }
            }
            ElseIf($testX -match "\(\d\d% loss") # match double digit loss
            {
                $result["Pass"] = $false
                $fontColour = "<font color='red'>"
                $startLocation = $location - 2
                $loss = $testX.SubString($startLocation, 2)
            }
            ElseIf($testX -match "\d\d\d% loss") # match triple digit loss (100%)
            {
                $result["Pass"] = $false
                $fontColour = "<font color='red'>"
                $loss = "100"
            }
         
            $latency = ($test | Select-String 'Minimum' | Out-String).Replace(", ","").Replace("Minimum = ", "</font><font color='black'>Min. Latency: </font>").Replace("Maximum = ", "<br/><font color='black'>Max. Latency: </font>").Replace("Average = ","<br/><font color='black'>Average Latency: </font>").Trim()
        
            $locationMinLatencyStart = ($test | Select-String 'Minimum' | Out-String).Trim().IndexOf("Minimum") + 10
            $locationMaxLatencyStart = ($test | Select-String 'Minimum' | Out-String).Trim().IndexOf("Maximum") + 10
            $locationAvgLatencyStart = ($test | Select-String 'Minimum' | Out-String).Trim().IndexOf("Average") + 10
        
            $locationMinLatencyEnd = $locationMaxLatencyStart - 14
            $locationMaxLatencyEnd = $locationAvgLatencyStart - 14
            $locationAvgLatencyEnd = ($test | Select-String 'Minimum' | Out-String).Trim().LastIndexOf("ms")
       
            $MinLatencyLength = $locationMinLatencyEnd - $locationMinLatencyStart
            $MaxLatencyLength = $locationMaxLatencyEnd - $locationMaxLatencyStart
            $AvgLatencyLength = $locationAvgLatencyEnd - $locationAvgLatencyStart

            $minLatency = [int]($test | Select-String 'Minimum' | Out-String).Trim().Substring($locationMinLatencyStart, $MinLatencyLength).Trim()
            $maxLatency = [int]($test | Select-String 'Minimum' | Out-String).Trim().Substring($locationMaxLatencyStart, $MaxLatencyLength).Trim()
            $avgLatency = [int]($test | Select-String 'Minimum' | Out-String).Trim().Substring($locationAvgLatencyStart, $AvgLatencyLength).Trim()
        
            $flutter = $maxLatency - $minLatency

            If([int]$avgLatency -gt 150) 
            {
                $result["Pass"] = $false
                $result["Result"] = "<font color='black'>Packet loss: </font><font color='red'>" `
                                    + $loss + "%" + $br + $latency `
                                    + "</font><br/> Your connectivity back to the REDACTED network is poor, this will affect your ability to use our systems and apps."
                $result["Remediation"] = "1) Check that 'Cellular' is turned off. Click the Start button, type 'cellular', and go to 'Change cellular settings'. Turn if off. See if this resolves your issues, otheriwse try the second step." `
                                         + "2) If connected to the VPN (REDACTED, bottom right of the taskbar), disconnect and connect via REDACTED (https://REDACTED.com.au).<br/>" 
            } 
            Else 
            {            
                $result["Result"] = "<font color='black'>Packet loss: </font>" + $fontColour + $loss + "%" + $br + $latency + "</font>"
                $result["Remediation"] = ""
            }
        } 
        Else 
        {
            $result["Pass"] = $false
            $result["Result"] = $test | Out-String
            $result["Remediation"] = ""
        }
    } 
    Else    
    { # match result without "Loss = " in it
        $result["Pass"] = $false
        $result["Result"] = $error[0].Exception | Out-String
    }

    Return $result
}

#example call pingLocation("www.REDACTED.com.au")
