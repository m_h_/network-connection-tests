﻿Function checkLANconnection {
    $result = @{}
    
    $test = netsh lan show interfaces name=Ethernet | Select-String "state"

    #Default results
    $result["Pass"] = $false
    $result["Result"] = "Network cable not unplugged"
    $result["Remediation"] = "Unplugging your ethernet cable will force wifi as the only networking option"
     
    ForEach($line in $test) {
        
        if($line -like "*Network cable unplugged*") {
            $result["Pass"] = $true
            $result["Result"] = "Network cable unplugged"
            $result["Remediation"] = ""
        }
    }

    Return $result
}
