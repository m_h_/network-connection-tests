﻿Function checkREDACTEDConnect {
    $result = @{}
    $test = (Get-Service -Name "vpnagent").Status
    
    $result["Pass"] = $false
    $result["Result"] = "'REDACTED' REDACTED not running"

    If($test -eq 'Running') { 
        $result["Pass"] = $true
        $result["Result"] = "'REDACTED' REDACTED Running"
    }

    Return $result
}
